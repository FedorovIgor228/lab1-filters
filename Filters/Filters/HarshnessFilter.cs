﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filters
{
    class HarshnessFilter : MatrixFilter
    {
        public HarshnessFilter()
        {
            int sizeX = 3;
            int sizeY = 3;
            kernel = new float[sizeX, sizeY];
            for (int i = 0; i < sizeX; i++)
                for (int j = 0; j < sizeY; j++)
                {
                    if ((i == 0 || i == 2) && (j == 0 || j == 1))
                        kernel[i, j] = 0;
                    else
                        kernel[i, j] = -1;
                    if (i == 1 && j == 1)
                        kernel[i, j] = 5;
                }
        }
    } 
}
