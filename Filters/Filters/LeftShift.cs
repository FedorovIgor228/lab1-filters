﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Filters
{
    class LeftShift:Filters
    {
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            Color sourceColor = sourceImage.GetPixel(x, y);
            int k = x + 50;
            int l = y;
            if (k >= sourceImage.Width)
                return Color.FromArgb(0,0,0);
            else
                return sourceImage.GetPixel(k, l);
        }
    }
}
