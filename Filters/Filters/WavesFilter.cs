﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Filters
{
    class WavesFilter : Filters
    {
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            Color sourceColor = sourceImage.GetPixel(x, y);
            int k = (int)(x + 20 * Math.Sin(2 * Math.PI * y / 60));
            int l = y;
            if (k >= sourceImage.Width)
                k = k % sourceImage.Width;
            if (k < 0)
                k = sourceImage.Width + k;
            return sourceImage.GetPixel(k, l);
        }
    }
   

}
