﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Filters
{
    class GrayWorldFilter : Filters
    {
        Color av;
        int averValue;
        private Color avreg(Bitmap source)
        {
            int sumR = 0;
            int sumG = 0;
            int sumB = 0;
            for (int i = 0; i < source.Height; i++)
                for (int k = 0; k < source.Width; k++)
                {
                    sumR += source.GetPixel(k, i).R;
                    sumG += source.GetPixel(k, i).G;
                    sumB += source.GetPixel(k, i).B;
                }
            Color result;
            int n = source.Height * source.Width;
            result = Color.FromArgb(sumR / n, sumR / n, sumB / n);
            return result;

        }
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            int r, g, b;
            if (x == 0 && y == 0)
            {
                av = avreg(sourceImage);
                averValue = (av.R + av.B + av.G) / 3;
            }
            if (av.R == 0)
                r = 0;
            else
                r = Clamp(sourceImage.GetPixel(x, y).R * averValue / av.R, 0, 255);
            if (av.G == 0)
                g = 0;
            else
                g = Clamp(sourceImage.GetPixel(x, y).G * averValue / av.G, 0, 255);
            if (av.B == 0)
                b = 0;
            else
                b = Clamp(sourceImage.GetPixel(x, y).B * averValue / av.B, 0, 255);
            Color result = Color.FromArgb(r, g, b);
            return result;
        }
    }
}
