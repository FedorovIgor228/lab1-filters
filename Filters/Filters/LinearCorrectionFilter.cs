﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Filters
{
    class LinearCorrectionFilter:Filters
    {
        private int YminR = 255, YmaxR = 0, YminG = 255, YmaxG = 0, YminB = 255, YmaxB = 0;
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            Color sourceColor = sourceImage.GetPixel(x, y);
            if (x == 0 && y == 0)
            {
                for (int i = 0; i < sourceImage.Width; i++)
                {
                    for (int j = 0; j < sourceImage.Height; j++)
                    {
                        Color pixColor = sourceImage.GetPixel(i, j);
                        if (YminR > pixColor.R) YminR = pixColor.R;
                        if (YmaxR < pixColor.R) YmaxR = pixColor.R;
                        if (YminG > pixColor.G) YminG = pixColor.G;
                        if (YmaxG < pixColor.G) YmaxG = pixColor.G;
                        if (YminB > pixColor.B) YminB = pixColor.B;
                        if (YmaxB < pixColor.B) YmaxB = pixColor.B;
                    }
                }
            }
            int resultR = sourceColor.R;
            int resultG = sourceColor.R;
            int resultB = sourceColor.R;
            if(YmaxR - YminR > 0)
                resultR = Clamp((int)((sourceColor.R - YminR) * 255 / (YmaxR - YminR)), 0, 255);
            if(YmaxG - YminG > 0)
                resultG = Clamp((int)((sourceColor.G - YminG) * 255 / (YmaxG - YminG)), 0, 255);
            if(YmaxB - YminB > 0)
                resultB = Clamp((int)((sourceColor.B - YminB) * 255 / (YmaxB - YminB)), 0, 255);
            Color resultColor = Color.FromArgb(resultR, resultG, resultB);
            return resultColor;
        }
    }
}
